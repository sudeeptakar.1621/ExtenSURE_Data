"DATABASE FILE."
# Importing the sqlite3 module.
import sqlite3

# Creating a database file called database.db.
conn = sqlite3.connect("database.db")

# Creating a table called users with the following columns:
# userId, password, email, firstName,
# lastName, address1, address2, zipcode, city, state, country, phone.
conn.execute(
    """CREATE TABLE users
        (userId INTEGER PRIMARY KEY autoincrement,
        password TEXT,
        email TEXT,
        firstName TEXT,
        lastName TEXT,
        address1 TEXT,
        address2 TEXT,
        zipcode TEXT,
        city TEXT,
        state TEXT,
        country TEXT,
        phone TEXT
    )"""
)

# Creating a table called products with the following columns:
# productId, name, price, description, image, stock, categoryId.
conn.execute(
    """CREATE TABLE products
    (productId INTEGER PRIMARY KEY,
    name TEXT,
    price REAL,
    description TEXT,
    image TEXT,
    stock INTEGER,
    categoryId INTEGER,
    FOREIGN KEY(categoryId) REFERENCES categories(categoryId)
    )"""
)

# Creating a table called kart with the following columns:
# userId, productId, FOREIGN KEY(userId)
# REFERENCES users(userId), FOREIGN KEY(productId)
# REFERENCES products(productId).
conn.execute(
    """CREATE TABLE kart
    (userId INTEGER,
    productId INTEGER,
    FOREIGN KEY(userId) REFERENCES users(userId),
    FOREIGN KEY(productId) REFERENCES products(productId)
    )"""
)

# Creating a table called categories with the following columns:
# categoryId, name.
conn.execute(
    """CREATE TABLE categories
    (categoryId INTEGER PRIMARY KEY,
    name TEXT
        )"""
)

# Closing the connection to the database.
conn.close()
