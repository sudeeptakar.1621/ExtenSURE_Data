"""All cart related operations present within in module."""

import sqlite3  # importing sqlite3 module for db operations
from flask import (redirect, render_template, request, session,
                   url_for, Blueprint)
from utility import get_login_details  # importing get_login_details function

_QUERY = "SELECT userId FROM users WHERE email = '"  # select data from users
_DATABASE_FILE = "database.db"  # creating a constant global variable
__INITIAL_VALUE = 0
__login_form = "authentication.login_form"
__ROW_VALUE = 2  # for subtotal
# creating CART_APP global variable used to route serveral below templates
# Bluepring module of flask is used to add current module to main server module
CART_APP = Blueprint(
    "cart_operations", __name__, static_folder="static",
    template_folder="templates"
)


@CART_APP.route("/addToCart")
def add_to_cart():
    """
    If the user is logged in, add the product to the cart
    :return: the redirect function.
    """
    response = ""  # temp varaible
    if "email" not in session:
        # if email not in session redirect in to login form
        response = redirect(url_for(__login_form))
    else:
        # assigned variable args to product_id local variable
        product_id = int(request.args.get("productId"))  # type: ignore

        # Established database connection as conn
        with sqlite3.connect(_DATABASE_FILE) as conn:
            cur = conn.cursor()  # creating cursor object

            # exectuting query on users table with in database
            cur.execute(_QUERY + session["email"] + "'")

            # fetching userid from users table
            # assigning it to user_id
            user_id = cur.fetchone()[int(__INITIAL_VALUE)]
            # executing isnert statement into kart table
            # with values of userid and productid
            insert_query = f"INSERT INTO kart (userId, productId)\
                 VALUES ({user_id}, {product_id})"
            try:
                cur.execute(insert_query)
            except ValueError as excep_var:
                conn.rollback()  # connection is rollback if value error
                msg = f"value error occured {excep_var}"
                print(msg)  # printing error message to console
            except Exception as excep_var:  # pylint: disable=broad-except
                conn.rollback()  # connection is rollback if value error
                msg = f"Error occured {excep_var}"
                print(msg)  # printing error message to console
        conn.commit()  # commiting changes to the database
        conn.close()  # closing connection of database is important
        response = redirect(url_for("root"))
    return response  # returning response


@CART_APP.route("/cart")
def cart():
    """
    It gets the user's email from the session,
    then uses it to get the user's ID from the database, then
    uses that ID to get the products in the user's cart from the database,
    then calculates the total nprice of those products, then renders
    the cart.html template with the products, total price, and
    other details
    :return: The cart.html template is being rendered.
    """
    response = ""
    # if email not in session redirect in to login form
    if "email" not in session:
        response = redirect(url_for(__login_form))
    else:

        # calling get_login_details functions.
        logged_in, first_name, no_of_items = get_login_details()
        email = session["email"]
        # Established database connection as conn
        with sqlite3.connect(_DATABASE_FILE) as conn:
            cur = conn.cursor()  # creating cursor object
            cur.execute(_QUERY + email + "'")
            user_id = cur.fetchone()[int(__INITIAL_VALUE)]
            # executing select statement on product table as per kart data
            cur.execute(
                "SELECT products.productId, products.name, products.price, \
                    products.image FROM products, kart WHERE \
                        products.productId = kart.productId AND kart.userId = "
                + str(user_id)
            )
            products = cur.fetchall()  # local variables
        # calculating total price
        total_price = sum(row[int(__ROW_VALUE)] for row in products)
        # returning the response to cart.html template
        response = render_template(
            "cart.html",
            products=products,
            totalPrice=total_price,
            loggedIn=logged_in,
            firstName=first_name,
            noOfItems=no_of_items,
        )
    return response


@CART_APP.route("/checkout")
def checkout():
    """
    If the user is not logged in, redirect them to the login page.
    Otherwise, get the user's details and
    the products in their cart, and render the checkout page
    :return: the rendered template of the checkout.html page.
    """
    response = ""
    # if email not in session redirect in to login form
    if "email" not in session:
        response = redirect(url_for(__login_form))
    else:
        # calling get_login_details functions.
        logged_in, first_name, no_of_items = get_login_details()
        email = session["email"]
        # Established database connection as conn
        with sqlite3.connect(_DATABASE_FILE) as conn:

            cur = conn.cursor()  # creating cursor object
            cur.execute(_QUERY + email + "'")
            user_id = cur.fetchone()[int(__INITIAL_VALUE)]
            # executing select statement on product table as per kart data
            cur.execute(
                "SELECT products.productId, products.name, \
                    products.price, products.image FROM \
                        products, kart WHERE products.productId \
                        = kart.productId AND kart.userId = "
                + str(user_id)
            )
            products = cur.fetchall()
        # calculating the total price as per the data items
        total_price = sum(row[int(__ROW_VALUE)] for row in products)
        # returning the response to the template checkout.html
        response = render_template(
            "checkout.html",
            products=products,
            totalPrice=total_price,
            loggedIn=logged_in,
            firstName=first_name,
            noOfItems=no_of_items,
        )
    return response


@CART_APP.route("/removeFromCart")
def remove_from_cart():
    """
    It deletes a row from the kart table where the userId and productId
    match the userId and productId
    passed in as arguments
    :return: a redirect to the root page.
    """
    response = ""
    # if email not in session redirect in to login form
    if "email" not in session:
        response = redirect(url_for(__login_form))
    email = session["email"]
    product_id = int(request.args.get("productId"))  # type: ignore
    # Established database connection as conn
    with sqlite3.connect(_DATABASE_FILE) as conn:
        cur = conn.cursor()  # creating cursor object
        cur.execute(_QUERY + email + "'")
        user_id = cur.fetchone()[int(__INITIAL_VALUE)]
        delete_query = f"DELETE FROM kart WHERE userId = {str(user_id)}\
             AND productId = {product_id}"
        try:
            # executing the delete query to kart w.r.t userid
            # and productid
            cur.execute(delete_query)
        except ValueError as excep_var:
            conn.rollback()  # In case of value error, rolling back
            msg = f"value error occured {excep_var}"
            print(msg)  # printing error message to the console
        except Exception as excep_var:  # pylint: disable=broad-except
            conn.rollback()  # printing error message to the console
            msg = f"Error occured {excep_var}"
            print(msg)  # printing error message to the console
    conn.commit()  # commiting changes to the database
    conn.close()  # closing database connection
    response = redirect(url_for("root"))  # returing the response to home
    return response
