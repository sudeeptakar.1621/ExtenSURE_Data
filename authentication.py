"""All authentication related functions present within in module."""

# from flask redirect, render_temp, request,
#  session and url for imported
# from flask Blueprint module is imported for
# adding current module to main
# server module
from flask import (Blueprint, redirect, render_template, request, session,
                   url_for)

# from utility module is_valid function is imported
from validations import is_valid

_DATABASE_FILE = "database.db"  # defining constant for .db file
_LOGIN_HTML = "login.html"  # defining login.html in global constant

# creating AUTH_APP variable to easily route serveral
# templates in current module
# using blueprint and adding it to the mail module
AUTH_APP = Blueprint(
    "authentication", __name__, static_folder="static",
    template_folder="templates"
)


@AUTH_APP.route("/loginForm")
def login_form():
    """
    If the user is already logged in, redirect them to the root page.
    Otherwise, render the login form
    :return: the rendered template of the login page.
    """
    response = ""  # creating empty string variable

    # if email key in session then
    # return and redirect to the homepage
    # as user is already logged in
    if "email" in session:
        response = redirect(url_for("root"))
    else:
        # if email not in session means user is not registered
        response = render_template(_LOGIN_HTML, error="")
    # return response to the login.html template
    return response


@AUTH_APP.route("/login", methods=["POST", "GET"])
def login():
    """
    If the request method is POST, then get the email and password
    from the form, and if the email and password are valid,
     then set the session email to the email
    and redirect to the root page.
    :return: the rendered template.
    """

    # If the request method is POST, then get the email and password
    # from the form, and if the email and password are valid,
    # then set the session email to the email
    if request.method == "POST":
        email = request.form["email"]
        password = request.form["password"]
        response = ""
        if is_valid(email, password):
            # if email and password is valid assign it to session
            session["email"] = email
            response = redirect(url_for("root"))
        else:
            # if not valid then send an error message
            error = "Invalid UserId / Password"
            response = render_template(_LOGIN_HTML, error=error)
        return response


@AUTH_APP.route("/logout")
def logout():
    """
    It removes the email key from the session dictionary and then
    redirects the user to the root route
    :return: the redirect function.
    """
    # removing email key from the sesson directory by
    # popping it and returing None
    session.pop("email", None)

    # redirecting to homepage after logging out
    return redirect(url_for("root"))
