"""UTITLITY FUNCTIONS USED BY SERVER>PY MODULE."""

import fnmatch  # imported fnmatch module
import sqlite3  # imported sqlite3 module for db operations

from flask import session  # imported session from flask module

_DATABASE_FILE = "database.db"  # db file assignment
_QUERY1 = "SELECT categoryId, name FROM categories"  # select from categories
__CONSTANT_ONE = 1
__CONSTANT_ZERO = 0


# Fetch user details if logged in
def get_login_details():
    """
    It returns a tuple containing a boolean value, a string and an integer
    :return: A tuple of three values:
    """
    # Established Database connection as conn
    with sqlite3.connect(_DATABASE_FILE) as conn:
        # The below code is creating a cursor object.
        cur = conn.cursor()
        if "email" not in session:  # condition check
            logged_in = False  # assigned False to logged in
            first_name = ""  # first_name is assigned an empty string
            no_of_items = int(__CONSTANT_ZERO)
        else:
            logged_in, first_name, no_of_items = fetch_login_details(cur)
    conn.close()  # closing database connection
    return (logged_in, first_name, no_of_items)


def get_items(search_query, cur):
    """
    It takes a search query and a cursor object as input and returns a list
    of categories that match the search query
    :param search_query: The search query entered by the user
    :param cur: cursor object
    :return: A list of categories that match the search query.
    """

    # executing select statement to category table into db
    cur.execute(_QUERY1)
    list_of_categories = []  # defining an empty list
    # for loop in fetched data from category table
    for each_category in cur.fetchall():
        if "," not in each_category[int(__CONSTANT_ONE)] and fnmatch.fnmatch(
            search_query.title(), f"*{each_category[int(__CONSTANT_ONE)]}*"
        ):
            # append into list of categries if , not in item data
            # and fnmatch matches as per the search
            list_of_categories.append(each_category[int(__CONSTANT_ZERO)])

        elif "," in each_category[int(__CONSTANT_ONE)]:
            # extend to list of categories in , in item data
            # fetched from categories
            temp_list = list(each_category[int(__CONSTANT_ONE)].split(","))
            list_of_categories.extend(
                each_category[int(__CONSTANT_ZERO)]
                for k in temp_list
                if fnmatch.fnmatch(search_query.title(), f"*{k}*")
                or fnmatch.fnmatch(k, f"*{search_query}*")
            )
    # returning the list_of_categories back to server module's search function
    return list_of_categories


def fetch_login_details(cur):
    """
    It fetches the login details of the user from the database and
     returns the details
    :param cur: cursor object
    :return: logged_in, first_name, no_of_items
    """
    logged_in = True  # assigned True value to logged_in var

    # executing select query in users table into the db
    cur.execute(
        "SELECT userId, firstName FROM users WHERE email = '"
        + session["email"]
        + "'"
    )
    # assigned item data fetched from users table into users_id
    # and first_name variable
    user_id, first_name = cur.fetchone()
    # executing select query on Kart table as per userid
    cur.execute(
        f"SELECT count(productId) FROM kart WHERE userId \
            = {str(user_id)}"
    )

    # fetching item data from Kart and assigned to no_of_items
    no_of_items = cur.fetchone()[int(__CONSTANT_ZERO)]
    return logged_in, first_name, no_of_items
