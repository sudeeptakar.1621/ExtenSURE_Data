"""VALIDATION FUNCTIONS USED BY SERVER>PY MODULE."""

import hashlib  # imported hashlib module for passwords hashing
import sqlite3  # imported sqlite3 module for db operations

_INITIAL_PARSE_VALUE = 0  # initialised parse value
_DATABASE_FILE = "database.db"  # db file assignment
_ALLOWED_EXTENSIONS = {"jpeg", "jpg", "png", "gif"}  # allowed extensions
__PARSE_SUBLIST_RANGE = 7
__CONSTANT_ONE = 1
__CONSTANT_ZERO = 0


def is_valid(email, password):
    """
    It connects to the database, gets all the rows from the users table,
    and then checks if the
    email and password match any of the rows. If they do, it returns True,
    otherwise it returns False.
    :param email: the email address of the user.
    :param password: the password that the user entered.
    :return: True or False.
    """

    # Establishing connection to the database
    conn = sqlite3.connect(_DATABASE_FILE)
    # creating cursor object
    cur = conn.cursor()
    # executing select statement in users table into the database
    cur.execute("SELECT email, password FROM users")
    # fetching the data items from users table into the data variable
    data = cur.fetchall()
    # fetching the password encoding from hashlib
    password = hashlib.md5(password.encode()).hexdigest()
    return any(
        row[int(__CONSTANT_ZERO)] == email
        and row[int(__CONSTANT_ONE)] == password
        for row in data
    )


def allowed_file(filename):
    """
    It returns true if the filename has a period in it
    and the part of the
    filename after the period is
    in the list of allowed extensions
    :param filename: The name of the file that was uploaded
    :return: a boolean value.
    """

    # checking if the file extentions allowed
    # as per the given _ALLOWED_EXTENSIONS
    return (
        "." in filename
        and filename.rsplit(".", int(__CONSTANT_ONE))[int(__CONSTANT_ONE)]
        in _ALLOWED_EXTENSIONS
    )


def parse(data):
    """
    It takes a list of numbers and returns a list of lists of numbers,
    where each sublist has 7 elements
    :param data: the data to be parsed
    :return: A list of lists.
    """

    ans = []  # creating an empty list ans
    temp_var = int(_INITIAL_PARSE_VALUE)
    while temp_var < len(data):
        curr = []  # creating an empty list curr
        for _ in range(int(__PARSE_SUBLIST_RANGE)):
            if temp_var >= len(data):
                break
            # append data values to curr list
            curr.append(data[temp_var])
            # increamenting initial parse value by 1
            temp_var += int(__CONSTANT_ONE)
        # appending ans list with curr list
        ans.append(curr)
    # returning parsed list of lists
    return ans  # returning a list of lists
