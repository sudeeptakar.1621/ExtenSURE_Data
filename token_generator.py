"TOKEN GENERATOR SCRIPT FOR CAST HL."
# Importing the requests module.
import requests

# Importing the constants from the constants.py file.
# Imported Base Url and Json data form constants module
from constants import BASE_URL, HEADERS
# Making a request to the API and getting the response.
_RESPONSE = requests.request(
    "GET",  # making a GET request method
    f"{BASE_URL}auth/oauth/authorize",  # BASE URL
    cookies=requests.post(
        f"{BASE_URL}WS/session", headers=HEADERS, timeout=86400
    ).cookies.get_dict(),  # including cookies fetched
    headers=HEADERS,
    timeout=86400,  # timeput is added to 30 seconds
)

_TOKEN = _RESPONSE.json()["token"]  # Getting the token from the response.
# Printing the token to the console.
print(_TOKEN)
