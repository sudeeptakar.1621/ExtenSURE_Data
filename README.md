# Flask-E-commerce
This is a simple e-commerce website in python Flask framework with payment gateway integration. It allows the user to signup/login, shop and then make the payment using Instamojo. I've hosted my application on Heroku. Click on this URL to test it out: https://flask-e-commerce-api-heroku.herokuapp.com/. 

# Pre-requisites
Python 3.9 and above
latest Python 3.10

pip 9.0.1
latest pip 23.0.1

Flask version: '0.12.2'
latest Flask '2.2.3'

Sqlite3

#commit for testing pipeline1



