"""All registration related functions present within in module."""

import hashlib  # hashlib module imported for password encode/decode
import sqlite3  # sqlite3 module imported for database operations

# from flask redirect, render_temp, request,
#  session and url for imported
# from flask Blueprint module is imported for
# adding current module to main
# server module
from flask import Blueprint, render_template, request

_DATABASE_FILE = "database.db"  # defining constant for .db file
_LOGIN_HTML = "login.html"  # defining login.html in global constant


# creating REGISTER_APP variable to easily route serveral
# templates in current module
# using blueprint and adding it to the mail module
REGISTER_APP = Blueprint(
    "registration", __name__, static_folder="static",
    template_folder="templates"
)


@REGISTER_APP.route("/register", methods=["GET", "POST"])
def register():
    """
    It takes the form data from the user and inserts it into
    the database.
    :return: The error message
    """
    # if request method is not equal to POST return None
    response = ""
    if request.method != "POST":
        response = None
    msg = update_password()
    # returning response to login html template with error message
    response = render_template(_LOGIN_HTML, error=msg)
    return response


@REGISTER_APP.route("/registerationForm")
def registration_form():
    """
    It renders the register.html file.
    :return: the render_template function.
    """
    # returing register.html tempalte for registration
    return render_template("register.html")


def update_password():
    """
    It takes the password from the form, hashes it, and
     inserts it into the database
    :return: the msg variable.
    """

    # Establishing Database connection as conn variable
    with sqlite3.connect(_DATABASE_FILE) as con:
        try:
            con.cursor()  # creating cursur connection object
        except ValueError as excep_var:
            con.rollback()  # rolling back connection if value error occured
            msg = f"value error occured {excep_var}"
        except Exception as excep_var:  # pylint: disable=broad-except
            con.rollback()  # rolling back connection if value error occured
            msg = f"Error occured {excep_var}"

            # executing INSERT statment in users table to register a user
        con.cursor().execute(
            "INSERT INTO users(password, email, firstName, lastName, address1,\
                 address2, zipcode, city, state, country, phone) \
                    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            (
                hashlib.md5((request.form["password"]).encode()).hexdigest(),
                request.form["email"],
                request.form["firstName"],
                request.form["lastName"],
                request.form["address1"],
                request.form["address2"],
                request.form["zipcode"],
                request.form["city"],
                request.form["state"],
                request.form["country"],
                request.form["phone"],
            ),
        )
    con.commit()  # commiting database changes
    msg = "Registered Successfully"
    con.close()  # closing database connection
    return msg
