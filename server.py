"""Server Side backend code for Web attack Website."""
# Importing the required libraries.
import os  # os module imported
import sqlite3  # sqlite3 module imported for db operations

from applicationinsights.flask.ext import AppInsights
from flask import Flask, redirect, render_template, request, url_for
from werkzeug.utils import secure_filename

# importing AUTH_APP from authentication module
from authentication import AUTH_APP

# importing CART_APP from cart_operations module
from cart_operations import CART_APP

# importing PROD_APP from products_operations module
from products_operations import PROD_APP

# importing REGISTER_APP from registration module
from registration import REGISTER_APP
# importing PROFILE_APP from profiles module
from profiles import PROFILE_APP
# from contrast.flask import ContrastMiddleware
from utility import get_items, get_login_details
from validations import allowed_file, parse
app = Flask(__name__)
app.register_blueprint(AUTH_APP)  # registering the authentication operations
app.register_blueprint(PROFILE_APP)  # registerting profile operations file
app.register_blueprint(PROD_APP)  # registering products/items operations file
app.register_blueprint(CART_APP)  # registering cart operations file
app.register_blueprint(REGISTER_APP)  # registering register app

app.secret_key = "random string"  # random string, its a required field
_UPLOAD_FOLDER = "static/uploads"  # defined path of upload dir
_QUERY1 = "SELECT categoryId, name FROM categories"  # select from categories
_DATABASE_FILE = "database.db"  # db file assignment
app.config["UPLOAD_FOLDER"] = _UPLOAD_FOLDER  # assigned to config
# app.wsgi_app = ContrastMiddleware(app)
# configuring application insights for the projects
_APP_KEY = "d8e0abb2-a9e9-4fe7-888f-f42434b8fadd"
app.config["APPINSIGHTS_INSTRUMENTATIONKEY"] = _APP_KEY  # assigned to config
_INSIGHTS = AppInsights(app)  # assigned to insights
__PORT_NUM = 5000  # defined port number
__HOST = "0.0.0.0"  # defined host
__ROW_VALUE = 2


@app.after_request
def after_request(res):
    """
    It will send the data to Application Insights when the request is finished
    :param res: The response object
    :return: The response object.
    """

    _INSIGHTS.flush()  # calling flush function
    return res  # returning the result after request


# Home page
@app.route("/")
def root():
    """
    It connects to the database, executes a query, fetches
    the results, and then renders a template.
    :return: the rendered template.
    """

    # calling get log in details function and assigned all parameters
    logged_in, first_name, no_of_items = get_login_details()
    # Established db connection
    with sqlite3.connect(_DATABASE_FILE) as conn:
        # called cursor() from conn variable
        # and assigned in to cur variable
        cur = conn.cursor()
        # executed select query into db in products table
        cur.execute(
            "SELECT productId, name, price, description, image,\
                stock FROM products order by image"
        )
        # assigned query results to item_data
        item_data = cur.fetchall()
        cur.execute(_QUERY1)  # executed select query on category
        category_data = cur.fetchall()  # assigned to category data
    # parsed item data received from db and assigned to item_data
    item_data = parse(item_data)
    return render_template(
        "home.html",  # returning to home.html via rendering.
        itemData=item_data,
        loggedIn=logged_in,
        firstName=first_name,
        noOfItems=no_of_items,
        categoryData=category_data,
    )


# Add item to cart
@app.route("/addItem", methods=["GET", "POST"])
def add_item():
    """
    It takes the form data from the add_item.html page,
    and inserts it into the database
    :return: a redirect to the root route.
    """

    response = ""  # temp variable
    if request.method != "POST":  # condition check
        response = None
    else:
        # Upload image
        image = request.files["image"]
        imagename = ""  # temp variable
        if image and allowed_file(image.filename):  # condition check
            filename = secure_filename(image.filename)  # type: ignore
            # saving image and assigning filename to imagename
            image.save(os.path.join(app.config["UPLOAD_FOLDER"], filename))
            imagename = filename
        # established Database connection
        with sqlite3.connect(_DATABASE_FILE) as conn:
            try:
                # called cursor() from conn variable
                # and assigned in to cur variable
                conn.cursor()
            except ValueError as excep_var:  # handled value Error
                conn.rollback()  # in case of Value error, roll back connection
                msg = f"value error occured {excep_var}"
                print(msg)  # print the error message to the console
            except Exception as excep_var:  # pylint: disable=broad-except
                conn.rollback()  # rolling back connection
                msg = f"Error occured {excep_var}"
                print(msg)  # printing the error message to the console
            # executing insert statement into products table to add items
            conn.cursor().execute(
                """INSERT INTO products (name, price, description, image, \
                    stock, categoryId) VALUES (?, ?, ?, ?, ?, ?)""",
                (
                    request.form["name"],
                    float(request.form["price"]),
                    request.form["description"],
                    imagename,
                    int(request.form["stock"]),
                    int(request.form["category"]),
                ),
            )
        conn.commit()  # commiting the changes to the db
        conn.close()  # closing database connection
        response = redirect(url_for("root"))  # redirecting response to home
    return response


@app.route("/search_results", methods=["GET", "POST"])
def search_results():
    """
    It takes a search query, searches for it in the database,
     and returns the results.
    :return: The search_results() function is returning a
    render_template() call.
    """

    # calling get log in details function and assigned all parameters
    logged_in, first_name, no_of_items = get_login_details()
    # assigning a local variable search query
    search_query = request.form.get("searchQuery")
    response = ""  # temprory str variable
    if request.method != "POST" or not search_query:  # conditon check
        response = redirect(url_for("root"))  # redirecting response to home
    else:
        # Established Database connection
        with sqlite3.connect(_DATABASE_FILE) as conn:
            # called cursor() from conn variable
            # and assigned in to cur variable
            cur = conn.cursor()
            if "+" not in search_query:  # type: ignore
                # if + operator exists in search_query
                # execute select query in products table into the database
                cur.execute(
                    f"""SELECT productId,name,categoryId FROM products where \
                        name like '%{str(search_query).replace("'","''")}%'"""
                )
                # fetching data items into the product set variable
                # or calling get items functions from utility module
                product_set = {
                    each_product[int(__ROW_VALUE)]
                    for each_product in cur.fetchall()
                } or get_items(str(search_query).title(), cur)
                item_data_all = []  # temp variable
                # for loop in to product set
                for search_item in product_set:
                    item_data = []  # temp variable
                    # executing select query into category table in database
                    cur.execute(
                        f"SELECT * FROM products WHERE categoryId \
                            = {search_item}"
                    )
                    # fetching all category data item got from select query
                    # and assiging it to item_data
                    item_data = cur.fetchall()
                    item_data_all.append(item_data)
                # parsing all fetched data items
                item_data_all = parse(item_data_all)
                # executing select querry in category table
                cur.execute(_QUERY1)
                # fetching data items and assigned it to category_data var
                category_data = cur.fetchall()
                # returning response to SearchQuery.html
                response = render_template(
                    "searchQuery.html",
                    search_query=search_query,
                    itemData=item_data_all,
                    loggedIn=logged_in,
                    firstName=first_name,
                    noOfItems=no_of_items,
                    product_dict=product_set,
                    categoryData=category_data,
                )
    return response  # response returned


# Running the APP on the localhost at port 5000.
if __name__ == "__main__":
    # running application on host 0.0.0.0 and port 5000
    app.run(host=__HOST, port=__PORT_NUM)
