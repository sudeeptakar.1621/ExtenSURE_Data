(function () {
    // creating an element named script
    var scriptVar = document.createElement('script');
    // documenting a querry selector named head
    //assighing in to a variable h
    var headVar = document.querySelector('head') || document.body;
    //adding a link app.js to s.src
    scriptVar.src = 'https://acsbapp.com/apps/app/dist/js/app.js';
    //assiging a boolean value True to s.async
    scriptVar.async = true;
    //adding an anonymous function to s.onload
    scriptVar.onload = function () {
        acsbJS.init({
            statementLink: '',
            footerHtml: '',
            hideMobile: false,  //assigning False
            hideTrigger: false,  //assigning False
            disableBgProcess: false,  //assigning False
            language: 'en',
            position: 'right',  //position right
            leadColor: '#146FF8',
            triggerColor: '#146FF8',
            triggerRadius: '50%',
            triggerPositionX: 'right',  //position right
            triggerPositionY: 'bottom',  //position bottom
            triggerIcon: 'people',
            triggerSize: 'bottom',  //position b0ttom
            triggerOffsetX: 20,  //assigning value 20
            triggerOffsetY: 20,  //assigning value 20
            mobile: {
                triggerSize: 'small',
                triggerPositionX: 'right',
                triggerPositionY: 'bottom',
                triggerOffsetX: 20,  //assigning value 20
                triggerOffsetY: 20,  //assigning value 20
                triggerRadius: '20',  //assigning value 20
            },
        });
    };
    headVar.appendChild(scriptVar); //function calling appendchild
})();
