/*
If the value of the password field is the same as the value 
of the confirm password field, then 
return true, otherwise alert the user that the passwords 
do not match and return false
@returns a boolean value.
*/
function validatePassword() {
/* Getting the value of the password field. */
    var passwordValidation = document.getElementById('password').value;
/* Getting the value of the confirm password field. */
    var confirmPassword = document.getElementById('cpassword').value;
/* Checking if the password and confirm password are the same. */
    if (passwordValidation == confirmPassword) {
        /* Returning a boolean value True. */
        return true;
    }
    else {
        /* Alerting the user that the passwords do not match. */
        alert('Passwords do not match');
        /* Returning a boolean value False. */
        return false;
    }
}
