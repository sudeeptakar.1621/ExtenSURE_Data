/*
  If the value of the password field is the same as the 
  value of the confirm password field, then
  return true, otherwise alert the user that the 
  passwords do not match and return false.
  @returns the value of the variable "pass" and "cpass"
 */
  function validatePassword() {
    /* Getting the value of the password field. */
    var passwordValidation = document.getElementById('newpassword').value;
    /* Getting the value of the confirm password field. */
    var confirmPassword = document.getElementById('cpassword').value;
    /* Checking if the password and confirm password are the same. */
    if (passwordValidation == confirmPassword) {
        /* Returning the value of the variable "pass" and "cpass" */
        return true;
    }
    /* Checking else part, the password and confirm password are the same. */
    else {
        alert('Passwords do not match!');
        /* Returning the value of the variable "pass" and "cpass" */
        return false;
    }
}
