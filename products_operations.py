"""All products/items related functions present within in module."""

import sqlite3  # imported sqlite3 module for db operations.

# imported redirect, render_template, request, url_for from flask module.
# imported Blueprint to add this module to server.py module.
from flask import Blueprint, redirect, render_template, request, url_for

# imported get_login_details and parse functions from utiliy module.
from utility import get_login_details
from validations import parse

_DATABASE_FILE = "database.db"  # creating a constant variable.
__ROW_VALUE = 0  # category_name row value in display category
__COLUMN_VALUE = 4  # category_name column value in display category

# creating an app variabled which is used to route to templates as per event.
PROD_APP = Blueprint(
    "products_operations", __name__, static_folder="static",
    template_folder="templates"
)


# Remove item from cart.
@PROD_APP.route("/removeItem")
def remove_item():
    """
    It deletes a product from the database based on the product ID
    :return: a redirect to the root page.
    """

    # creating a local variable product_id.
    product_id = request.args.get("productId")
    # Establishing database connection as conn variable.
    with sqlite3.connect(_DATABASE_FILE) as conn:
        # creating cursor object from conn.cursor().
        cur = conn.cursor()
        try:
            # exectuting delete query from product table as per productid.
            cur.execute(f"DELETE FROM products WHERE productID = {product_id}")
        except ValueError as excep_var:
            conn.rollback()
            msg = f"value error occured {excep_var}"
            print(msg)  # printing the error message to the console.
        except Exception as excep_var:  # pylint: disable=broad-except.
            conn.rollback()  # rolling back connection.
            msg = f"Error occured {excep_var}"
            print(msg)  # printing the error message to the console.
    conn.commit()  # commiting changes to the database.
    msg = "Deleted successsfully"  # local string variable for msg.
    conn.close()  # closing the connection of database.
    return redirect(url_for("root"))


# Display all items of a category.
@PROD_APP.route("/displayCategory")
def display_category():
    """
    It displays the products in a category
    :return: the rendered template.
    """

    # calling get_login_details functions.
    logged_in, first_name, no_of_items = get_login_details()
    # establishing db connection as conn variable.
    with sqlite3.connect(_DATABASE_FILE) as conn:
        # creating cursor object.
        cur = conn.cursor()
        # exectuting select query into products table.
        cur.execute(
            "SELECT products.productId, products.name, \
                products.price, products.image,categories.name\
                     FROM products, categories WHERE \
                    products.categoryId = categories.categoryId\
                        AND categories.categoryId = "
            + request.args.get("categoryId")  # type: ignore
        )
        data = cur.fetchall()
    conn.close()  # closing the database connection is important
    category_name = data[int(__ROW_VALUE)][int(__COLUMN_VALUE)]
    data = parse(data)  # parsing the data
    return render_template(
        "displayCategory.html",
        data=data,
        loggedIn=logged_in,
        firstName=first_name,
        noOfItems=no_of_items,
        categoryName=category_name,
    )


@PROD_APP.route("/productDescription")
def product_description():
    """
    It gets the product id from the URL, connects to the database,
    gets the product data from the database, and then renders the product
    description page with the product data.
    :return: The product_data is being returned.
    """

    # calling get_login_details functions.
    logged_in, first_name, no_of_items = get_login_details()
    # local variable product_id assigned argument variables.
    product_id = request.args.get("productId")
    # establishing db connections.
    with sqlite3.connect(_DATABASE_FILE) as conn:
        # creating cursor object.
        cur = conn.cursor()
        cur.execute(
            "SELECT productId, name, price, description, image, stock \
                FROM products WHERE productId = "
            + product_id  # type: ignore
        )
        # fetching details to product_data variable.
        product_data = cur.fetchone()
    conn.close()  # closing connection of db.
    # returing the response to productDescription.html.
    return render_template(
        "productDescription.html",
        data=product_data,
        loggedIn=logged_in,
        firstName=first_name,
        noOfItems=no_of_items,
    )


@PROD_APP.route("/instamojo")
def instamojo():
    """
    It renders the instamojo.html file.
    :return: the rendered template.
    """

    # payment gateway.
    return render_template("instamojo.html")
