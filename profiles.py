"""All profiles related functions present within in module."""

import hashlib  # hashlib module imported for password encode/decode
import sqlite3  # sqlite3 module imported for database operations

# from flask redirect, render_temp, request, session and url for imported
# from flask Blueprint module is imported for adding current module to main
# server module
from flask import (Blueprint, redirect, render_template, request, session,
                   url_for)

# from utility module get_login_details function is imported
from utility import get_login_details

_DATABASE_FILE = "database.db"  # defining constant for .db file
__CHANGE_PASSWORD = "changePassword.html"

# creating PROFILE_APP variable to easily route serveral
# templates in current module
# using blueprint and adding it to the mail module
PROFILE_APP = Blueprint(
    "profiles", __name__, static_folder="static", template_folder="templates"
)


@PROFILE_APP.route("/account/profile")
def profile_home():
    """
    If the user is not logged in, redirect them to the root page.
    If they are logged in, render the profileHome.html template
    :return: the rendered template.
    """
    response = ""
    # if email not in session redirect in to homepage
    if "email" not in session:
        # redirecting the response to home page
        response = redirect(url_for("root"))
    else:
        # calling get_login_details functions.
        logged_in, first_name, no_of_items = get_login_details()
        # returning response to profilehome.html
        # including required parameters with in the response
        response = render_template(
            "profileHome.html",
            loggedIn=logged_in,
            firstName=first_name,
            noOfItems=no_of_items,
        )
    return response


@PROFILE_APP.route("/account/profile/edit")
def edit_profile():
    """
    If the user is not logged in, redirect them to the root page.
    Otherwise, get the user's profile data from the database and
    render the
    editProfile.html template with the profile data.
    :return: the rendered template.
    """
    response = ""
    # if email not in session redirect in to homepage
    if "email" not in session:
        # return the response to the root i.e. homepage
        response = redirect(url_for("root"))
    else:
        # calling get_login_details functions.
        logged_in, first_name, no_of_items = get_login_details()
        # Establishing database connection as conn
        with sqlite3.connect(_DATABASE_FILE) as conn:
            cur = conn.cursor()  # creating cursor object of db conn

            # executing select query to users table to fetch users table
            cur.execute(
                "SELECT userId, email, firstName, lastName, address1,\
                     address2, zipcode, city, state, country, phone\
                         FROM users WHERE email = '"
                + session["email"]
                + "'"
            )

            # fetched user tables are stored in profile_data variable
            profile_data = cur.fetchone()
        conn.close()  # closing connection database

        # returning the response to editprofile.html template
        # including required variables
        response = render_template(
            "editProfile.html",
            profileData=profile_data,
            loggedIn=logged_in,
            firstName=first_name,
            noOfItems=no_of_items,
        )
    return response  # returned response


@PROFILE_APP.route("/account/profile/change_password", methods=["GET", "POST"])
def change_password():
    """
    If the user is logged in, and the old password is correct,
    then change the password to the new password.
    :return: the rendered template.
    """
    # if email not in session redirect in to login form
    response = ""
    if "email" not in session:
        response = redirect(url_for("authentication.login_form"))
    if request.method != "POST":
        response = render_template(__CHANGE_PASSWORD)
    else:
        old_password = request.form["oldpassword"]
        old_password = hashlib.md5(old_password.encode()).hexdigest()
        new_password = request.form["newpassword"]
        new_password = hashlib.md5(new_password.encode()).hexdigest()
        msg = ""
        # Establishing database connection as conn
        with sqlite3.connect(_DATABASE_FILE) as conn:
            cur = conn.cursor()  # creating cursor object of db conn
            # executing select statement in users table to fetch dataitems
            cur.execute(
                "SELECT userId, password FROM users WHERE email = '"
                + session["email"]
                + "'"
            )
            # data items from user tables are fetched
            # and assigned to user_id and password varaible
            user_id, password = cur.fetchone()

            # condition check if password is equal to old password
            if password == old_password:
                update_query = f"UPDATE users SET password = {new_password}\
                     WHERE userId = {user_id}"
                try:
                    cur.execute(update_query)
                except ValueError as excep_var:
                    conn.rollback()  # rolling back connection
                    msg = f"value error occured {excep_var}"
                except Exception as excep_var:  # pylint: disable=broad-except
                    conn.rollback()  # rolling back connection
                    msg = f"Error occured {excep_var}"
            else:
                msg = "Wrong password"
        conn.commit()  # commiting password change tthe database
        msg = "Changed successfully"
        conn.close()  # closing database connection
        # redirecting response back to changepassword.html with error
        # or success message
        response = render_template(__CHANGE_PASSWORD, msg=msg)
    return response  # type: ignore


@PROFILE_APP.route("/updateProfile", methods=["GET", "POST"])
def update_profile():
    """
    It updates the user's profile information in the database.
    :return: the redirect function.
    """

    # return None if request.method not equal to POST
    response = ""
    if request.method != "POST":
        response = None
    else:
        # Establishing database connection as conn
        with sqlite3.connect(_DATABASE_FILE) as conn:
            try:
                conn.cursor()  # creating cursor object of db conn
            except ValueError as excep_var:
                conn.rollback()  # rolling back connection if value error
                msg = f"value error occured {excep_var}"
                print(msg)  # printing error message to the console
            except Exception as excep_var:  # pylint: disable=broad-except
                conn.rollback()  # rolling back connection if value error
                msg = f"Error occured {excep_var}"
                print(msg)  # printing error message to the console
                # executing UPDATE statement to users table below to update
                # profile as per the users want
            conn.cursor().execute(
                "UPDATE users SET firstName = ?, lastName = ?, address1 = ?, \
                    address2 = ?, zipcode = ?, city = ?, state = ?, \
                        country = ?, phone = ? WHERE email = ?",
                (
                    request.form["firstName"],
                    request.form["lastName"],
                    request.form["address1"],
                    request.form["address2"],
                    request.form["zipcode"],
                    request.form["city"],
                    request.form["state"],
                    request.form["country"],
                    request.form["phone"],
                    request.form["email"],
                ),
            )
        conn.commit()  # commiting database changes to .db file
        msg = "Profile Updated successsfully"  # local string variable for msg.
        conn.close()
        response = redirect(url_for("profiles.edit_profile"))
    # redirecting the response back to the editprofile template
    return response
